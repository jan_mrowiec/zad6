//bitbucket.org/jan_mrowiec/zad6/src/master/zad6.cpp
#include <iostream>
#include <vector>
#include <cstdlib>
#include <cstdio>
#include <ctime>
/*
    Zasada dzialania.
    W zmiennej typu Flota program przetrzymuje dowolna ilosc statkow. Nastepnie przez okreslona ilosc dni sprawuje raport o ich stanie.
    Kazdy statek posiada licznik zasabow na pokladzie. Kazdego dnia wsrod osob na statku sa rozdzielana racje zywieniowe z zasobow.
    Jezeli ilosc zasobow na statku spadnie do 0 statek idzie na dno i znika z floty.
    Racje zaleznie od rodzaju statku sa dzielone na:
    Statek: miedzy zalogantami
    Statek Pasazerski: miedzy zalogantami i pasazerami
    Mysliwiec: miedzy zalogantami
    Transportowiec: miedzy zalogantami pasazerami i zolnierzami

    Flota ma szanse losowe na dobicie do ladu, gdzie moze uzupelnic zapasy. Lad dzieli sie na:
    Brzeg - mozliwosc uzupelnienia zasabow
    Osada Rybacka - mozliwosc uzupelnienia zasabow, oraz zalogi
    Port - mozliwosc uzupelnienia zasobow, zalogi oraz pasazerow

    Uzupelnienia sa zawsze dzielone rowno miedzy statki.
    Flota zawsze stara sie po dobiciu do brzegu wziac 100 kazdej ilosci zasabow.
*/
using namespace std;

class Kapitan
{
    const string imie;
    const string nazwisko;
    int staz;
public:
    string get_imie()const
    {
        return imie;
    }
    string get_nazwisko()const
    {
        return nazwisko;
    }
    int get_staz()const
    {
        return staz;
    }
    void increase_staz()
    {
        staz++;
    }
    Kapitan(string imie, string nazwisko, int staz): imie(imie), nazwisko(nazwisko), staz(staz){}
    ~Kapitan(){}
    Kapitan(const Kapitan& k):imie(k.get_imie()), nazwisko(k.get_nazwisko()), staz(k.get_staz()) {}
    friend ostream& operator<<(ostream& strumien, const Kapitan& statek);
};
ostream& operator<<(ostream& strumien, const Kapitan& kapitan)
{
    strumien << "Kapitan: " << kapitan.get_imie() << " " << kapitan.get_nazwisko() << ". Lat na morzu: " << kapitan.get_staz();
    return strumien;
}






class Statek
{
    Kapitan kapitan;
    const string nazwa;
    int zaloga;
    mutable int liczba_wywolan_get_zaloga = 0;
    int id;
    int racje;
    static int obecne_id;
    int zywnosc;
public:
    int get_zaloga()const
    {
        liczba_wywolan_get_zaloga++;
        return zaloga;
    }
    void set_zaloga(int zaloga)
    {
        this->zaloga = zaloga;
    }
    Statek(Kapitan kapitan, string nazwa, int zaloga, int zywnosc):kapitan(kapitan), nazwa(nazwa), zaloga(zaloga), zywnosc(zywnosc)
    {
        id = obecne_id++;
        oblicz_zywnosc();
    }
    Statek():kapitan(Kapitan("Nie", "Znany", 0))
    {
        id = obecne_id++;
        zaloga = 0;
    }
    void werbunek(int liczba)
    {
        zaloga += liczba;
    }
    int get_id()const
    {
        return id;
    }
    string get_nazwa()const
    {
        return nazwa;
    }
    Kapitan get_kapitan()const
    {
        return kapitan;
    }
    virtual void oblicz_zywnosc()
    {
        racje = 3 * zaloga;
    }
    bool n_dzien()
    {
        zywnosc -= racje;
        if(zywnosc >= 0)
            return true;
        else
            return false;
    }
    int get_zywnosc()const
    {
        return zywnosc;
    }
    void set_zywnosc(int zywnosc)
    {
        this->zywnosc = zywnosc;
    }
    void set_racje(int racje)
    {
        this->racje = racje;
    }
    Statek(const Statek& s):kapitan(s.get_kapitan()), nazwa(s.get_nazwa()), zaloga(s.get_zaloga()), zywnosc(s.get_zywnosc())
    {
        id = obecne_id++;
    }
    virtual ~Statek(){}
    Statek& operator=(const Statek& s)
    {
        Statek* out = new Statek(s);
        return *out;
    }
    friend ostream& operator<<(ostream& strumien, const Statek& statek);
};
ostream& operator<<(ostream& strumien, const Statek& statek)
{
    strumien << "Nazwa: " << statek.get_nazwa() << endl;
    strumien << statek.kapitan << endl;
    strumien << "Liczba zapasow: " << statek.get_zywnosc() << endl;
    strumien << "Liczba zalogantow: " << statek.get_zaloga() << endl;
    return strumien;
}
int Statek::obecne_id = 0;


class Statek_Pasazerski : public virtual Statek
{
    int pasazerowie;
public:
    int liczba_osob()const
    {
        return pasazerowie + get_zaloga();
    }
    void set_pasazerowie(int p)
    {
        pasazerowie = p;
    }
    int get_pasazerowie()const
    {
        return pasazerowie;
    }
    virtual void oblicz_zywnosc()
    {
        set_racje(3 * (get_zaloga() + pasazerowie));
    }
    Statek_Pasazerski(Kapitan kapitan, string nazwa, int zaloga, int zywnosc, int pasazerowie):Statek(kapitan, nazwa, zaloga, zywnosc), pasazerowie(pasazerowie){oblicz_zywnosc();}
    virtual ~Statek_Pasazerski(){}
    friend ostream& operator<<(ostream& strumien, const Statek_Pasazerski& statek);
};
ostream& operator<<(ostream& strumien, const Statek_Pasazerski& statek)
{
    Statek s = statek;
    strumien << s;
    strumien << "Liczba pasezerow: " << statek.get_pasazerowie() << endl;
    return strumien;
}

class Mysliwiec:public virtual Statek
{
    int ilosc_wiezyczek;
public:
    int get_ilosc_wiezyczek()const
    {
        return ilosc_wiezyczek;
    }
    void set_ilosc_wiezyczek(int w)
    {
        ilosc_wiezyczek = w;
    }
    Mysliwiec(Kapitan kapitan, string nazwa, int zaloga, int zywnosc, int ilosc_wiezyczek):Statek(kapitan, nazwa, zaloga, zywnosc), ilosc_wiezyczek(ilosc_wiezyczek){}
    virtual ~Mysliwiec(){}
    friend ostream& operator<<(ostream& strumien, const Mysliwiec& statek);
};
ostream& operator<<(ostream& strumien, const Mysliwiec& statek)
{
    Statek s = statek;
    strumien << s;
    strumien << "Liczba wierzyczek: " << statek.get_ilosc_wiezyczek() << endl;
    return strumien;
}
class TransportowiecBojowy:public Statek_Pasazerski, public Mysliwiec
{
    int ilosc_zolnierzy;
public:

    int get_ilosc_zolnierzy()const
    {
        return ilosc_zolnierzy;
    }
    void set_ilosc_zolnierzy(int w)
    {
        ilosc_zolnierzy = w;
    }
    virtual void oblicz_zywnosc()
    {
        set_racje(3 * (get_zaloga() + get_pasazerowie() + ilosc_zolnierzy));
    }
    TransportowiecBojowy(Kapitan kapitan, string nazwa, int zaloga, int zywnosc, int ilosc_wiezyczek, int pasazerowie, int ilosc_zolnierzy):Mysliwiec(kapitan, nazwa, zaloga, zywnosc, ilosc_wiezyczek),
    Statek_Pasazerski(kapitan, nazwa, zaloga, zywnosc, pasazerowie), Statek(kapitan, nazwa, zaloga, zywnosc), ilosc_zolnierzy(ilosc_zolnierzy){oblicz_zywnosc();}
    virtual ~TransportowiecBojowy(){}
    friend ostream& operator<<(ostream& strumien, const TransportowiecBojowy& statek);
};
ostream& operator<<(ostream& strumien, const TransportowiecBojowy& statek)
{
    Statek_Pasazerski s = statek;
    strumien << s;
    strumien << "Liczba wierzyczek: " << statek.get_ilosc_wiezyczek() << endl;
    strumien << "Liczba zolnierzy: " << statek.get_ilosc_zolnierzy() << endl;
    return strumien;
}









class Flota
{
    int ilosc_pieniedzy;
    vector<Statek*> statki;
public:
    void dodaj_statek(Statek* x)
    {
        statki.push_back(x);
    }
    Statek* get_statek(int i)
    {
        return statki[i];
    }
    void wyswietl_statki()
    {
        vector<Statek*>::iterator it = statki.begin();
        for(; it != statki.end(); it++)
        {
            TransportowiecBojowy *ptr_tb = dynamic_cast<TransportowiecBojowy*>(*it);
            if(ptr_tb != NULL)
            {
                cout << "Typ: Transportowiec Bojowy" << endl;
                cout << *ptr_tb << endl;
                continue;
            }
            Mysliwiec *ptr_m = dynamic_cast<Mysliwiec*>(*it);
            if(ptr_m != NULL)
            {
                cout << "Typ: Mysliwiec" << endl;
                cout << *ptr_m << endl;
                continue;
            }
            Statek_Pasazerski *ptr_sp = dynamic_cast<Statek_Pasazerski*>(*it);
            if(ptr_sp != NULL)
            {
                cout << "Typ: Statek pasazerski" << endl;
                cout << *ptr_sp << endl;
                continue;
            }
            cout << "Typ: Statek" << endl;
            cout << **it << endl;
        }
    }
    bool n_dzien()
    {
        vector<Statek*>::iterator it = statki.begin();
        for(; it != statki.end(); it++)
        {
            if(!(*it)->n_dzien())
            {
                cout << (*it)->get_nazwa() << " skonczyly sie zapasy. Statek poszedl na dno!!!!!!" << endl;
                delete *it;
                statki.erase(it);
                it--;
            }
        }
        if(statki.size() == 0)
            return false;
        else
            return true;
    }
    void dodaj_zapasy(int zapasy)
    {
        zapasy = zapasy / statki.size();
        vector<Statek*>::iterator it = statki.begin();
        for(; it != statki.end(); it++)
        {
            int z_statku = (*it)->get_zywnosc();
            (*it)->set_zywnosc(zapasy + z_statku);
        }
    }
    void dodaj_zaloge(int zaloga)
    {
        zaloga = zaloga / statki.size();
        vector<Statek*>::iterator it = statki.begin();
        for(; it != statki.end(); it++)
        {
            (*it)->werbunek(zaloga);
        }
    }
    void dodaj_pasazerow(int p)
    {
        for(int i = 0; i < statki.size() ; i++)
        {
            Statek *ptr_h = statki.at(i);
            Statek_Pasazerski *ptr = dynamic_cast<Statek_Pasazerski*>(ptr_h);
            if(ptr != NULL)
            {
                if(ptr->get_pasazerowie() + p > 100)
                {
                    p -= 100 - ptr->get_pasazerowie();
                    ptr->set_pasazerowie(100);
                }
                else
                {
                    ptr->set_pasazerowie(p + ptr->get_pasazerowie());
                    return;
                }
            }
        }
    }

    ~Flota(){
        vector<Statek*>::iterator it = statki.begin();
        for(; it != statki.end(); it++)
            delete *it;
    }
};









class Brzeg
{
    int ilosc_zapasow = 50;
public:
    int daj_zapasy(int x)
    {
        if(x > ilosc_zapasow)
        {
            int h = ilosc_zapasow;
            ilosc_zapasow = 0;
            return h;
        }
        else
        {
            ilosc_zapasow -= x;
            return x;
        }
    }
    int get_ilosc_zapasow()const
    {
        return ilosc_zapasow;
    }
    ~Brzeg(){}
};
class Osada_rybacka:public Brzeg
{
    int ilosc_osadnikow = 10;
public:
    int werbuj(int x)
    {
        if(x > ilosc_osadnikow)
        {
            x -= ilosc_osadnikow;
            int h = ilosc_osadnikow;
            ilosc_osadnikow = 0;
            return h;
        }
        else
        {
            int h = x;
            ilosc_osadnikow -= x;
            x = 0;
            return h;
        }
    }
    ~Osada_rybacka(){}
};
class Port:public Osada_rybacka
{
    int ilosc_pasazerow = 20;
public:
    int przewiez(int x)
    {
        if(x > ilosc_pasazerow)
        {
            int h = ilosc_pasazerow;
            ilosc_pasazerow = 0;
            return h;
        }
        else
        {
            ilosc_pasazerow -= x;
            return x;
        }
    }
    ~Port(){}
};


void przykladowe_wywolanie(Flota &flota, int& ilosc_dni)
{
    Statek *s1 = new Statek(Kapitan("Jacek", "Wrona", 2), "Czarna Perla", 10, 200);
    Statek *s2 = new Statek_Pasazerski(Kapitan("Czarno", "Brody", 2), "Zemsta Krolowej Anny", 10, 3000, 20);
    Statek *s3 = new Mysliwiec(Kapitan("Vincet", "Barbarossa", 2), "Latajacy holender", 10, 600, 15);
    Statek *s4 = new TransportowiecBojowy(Kapitan("Pawel", "Kowalski", 2), "Lotniskowiec b1", 10, 3000, 10, 40, 2);
    flota.dodaj_statek(s1);
    flota.dodaj_statek(s2);
    flota.dodaj_statek(s3);
    flota.dodaj_statek(s4);
    ilosc_dni = 10;
}

void wlasne_wywolanie(Flota &flota, int& ilosc_dni)
{
    cout << "Wpisz ilosc dni rejsu" << endl;
    cin >> ilosc_dni;
    cout << "Wpisz ilosc statkow" << endl;
    int len;
    cin >> len;
    for(int i = 0; i < len; i++)
    {
        cout << "Statek " << i + 1 << endl;
        cout << "Rodzaj statku" << endl;
        cout << "[1]Zwykly statek" << endl;
        cout << "[2]Statek Pasazerski" << endl;
        cout << "[3]Mysliwiec" << endl;
        cout << "[4]Transportowiec Bojowy" << endl;
        int st;
        cin >> st;
        string imie, nazwisko;
        cout << "Wprowadz imie i nazwisko kapitana" << endl;
        cin >> imie >> nazwisko;
        cout << "Wprowadz staz na morzu kapitana" << endl;
        int staz;
        cin >> staz;
        cout << "Wprowadx nazwe statku" << endl;
        string nazwa;
        cin >> nazwa;
        cout << "Wprowadz liczbe zalogantow" << endl;
        int zaloga;
        cin >> zaloga;
        cout << "Wprowadz liczbe zapasow na statku" << endl;
        int zapasy;
        cin >> zapasy;
        if(st == 1)
        {
            flota.dodaj_statek(new Statek(Kapitan(imie, nazwisko, staz), nazwa, zaloga,zapasy));
            continue;
        }
        int pass;
        if(st == 2 || st == 4)
        {
            cout << "Wprowadz liczbe pasazerow" << endl;
            cin >> pass;
            if(st == 2)
            {
                flota.dodaj_statek(new Statek_Pasazerski(Kapitan(imie, nazwisko, staz), nazwa, zaloga,zapasy, pass));
                continue;
            }
        }
        cout << "Wprowadz liczbe wierzyczek" << endl;
        int wiez;
        cin >> wiez;
        if(st == 3)
        {
                flota.dodaj_statek(new Mysliwiec(Kapitan(imie, nazwisko, staz), nazwa, zaloga,zapasy, wiez));
                continue;
        }
        cout << "Wprowadz liczbe zolnierzy" << endl;
        int zol;
        cin >> zol;
        flota.dodaj_statek(new TransportowiecBojowy(Kapitan(imie, nazwisko, staz), nazwa, zaloga,zapasy, wiez, pass, zol));
    }
}

int main()
{
    srand(time(NULL));
    Flota flota;
    int ilosc_dni;
    cout << "[1] Przykladowe wywolanie" << endl;
    cout << "[2] Wlasne wywolanie" << endl;
    int ch;
    cin >> ch;
    if(ch == 2)
        wlasne_wywolanie(flota, ilosc_dni);
    else
        przykladowe_wywolanie(flota, ilosc_dni);
    cout << "Dziennik kapitanski" << endl;
    cout << "--------------------------------------------------------------" << endl;
    for(int i = 0; i < ilosc_dni; i++)
    {
        cout << "--------------------------------------------------------------" << endl;
        cout << "Dzien " << i + 1 << endl;
        int random = rand() % 6;
        if(random == 0) //brzeg
        {
            cout << "Dotarles do brzegu. Uzepelniasz zapasy." << endl;

            Brzeg b;
            int zapasy = b.daj_zapasy(100);
            flota.dodaj_zapasy(zapasy);
        }
        else if(random == 1)//osada
        {
            cout << "Dotarles do osady rybackiej. Uzepelniasz zapasy i zaloge na statku" << endl;
            Osada_rybacka osada;
            int zapasy = osada.daj_zapasy(100);
            flota.dodaj_zapasy(zapasy);
            int werbuj = osada.werbuj(100);
            flota.dodaj_zaloge(werbuj);
        }
        else if(random == 2)//port
        {
            cout << "Dotarles do portu. Uzepelniasz zapasy, zaloge na statku oraz okretujesz nowych pasazerow" << endl;
            Port port;
            int zapasy = port.daj_zapasy(100);
            flota.dodaj_zapasy(zapasy);
            int werbuj = port.werbuj(100);
            flota.dodaj_zaloge(werbuj);
            int pass = port.przewiez(100);
            flota.dodaj_pasazerow(pass);
        }
        else//kolejny dzien na morzu
        {
            cout << "Kolejny dzien na morzu..." << endl << endl;
        }
        cout << "Raport:" << endl;
        if(!flota.n_dzien())
        {
            cout << "Cala twoja flota poszla na dno" << endl;
            cout << "Koniec" << endl;
            return 0;
        }
        flota.wyswietl_statki();
    }
    cout << "Dotarles do konca swojej podrozy" << endl;
    return 0;
}
